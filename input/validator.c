// ===--------------------------------=== //
//			  	          //
// test program to validate the features  //
// 					  //
// ===--------------------------------=== //

#include <stdio.h>
#include <stdbool.h>

extern int printf(const char *fmt, ...);

/* function to generate an indirect call */
static void foo() {
  printf("Hello!\n");
}

/* function phi generates phi node */
void phi(bool phi01,bool phi02)
{
	bool l = phi01 || phi02;
}

/* binary integer operations */
void binary_integer(int x, int y)
{
	int add = x + y;
	int sub = x - y;
	int pmult = x * y;
	int div = (int)(x/y); 
}
/* binary float operations */
void binary_float(float x, float y)
{
	float padd = x + y;
	float psub = x - y;
	float pmult = x * y;
	float pdiv = x/y;
}

/* function related to pointer arithmetic and ptr returned value */
int* pfunc(int pt1, int pt2)
{
	int* ptr1 = &pt1;
	int* ptr2 = &pt2;
	ptr1 = ptr1 + 3;
	ptr2 = ptr2 - 3;

	return ptr1;
}

/* function with more than 4 arguments */
int args(int arg1, int arg2, int arg3, int arg4, int arg5)
{
	return arg1;
}

const int ptrarg(const int* pt)
{
	return *pt;		
}

int main()
{
	static int s1 = 20;
	static int s2 = 10;
 
	/* reference to static/extern variable */
	const int* v1 = &s1;

	/* call with num. of args. greater than 4 */
   	/* call that return an integer */
	int ft = args(0,5,15,20,30);

	/* call that return a pointer */
	int* plft = pfunc(1,2);

	/* taking the address of a function */
  	void (*pointer)(void) = foo;
	int* (*lfunc)(int pt1, int pt2) = &pfunc;
	
	/* indirect function call */
  	ptrarg(v1);
	pointer();

	/* direct function call && function call with ptr as argument */
	int direct = ptrarg(v1);
	return ft;
}
