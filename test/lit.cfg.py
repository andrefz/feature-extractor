import platform
import lit.formats

from lit.llvm import llvm_config
from lit.llvm.subst import ToolSubst

config.name = "feature-extractor"
config.suffixes = [".ll"]
config.test_format = lit.formats.ShTest(not llvm_config.use_lit_shell)
config.test_source_root = os.path.dirname(__file__)

extra_args_darwin = ["-isysroot", "`xcrun --show-sdk-path`"]
extra_args = extra_args_darwin if platform.system() == "Darwin" else []
subst = ToolSubst("%clang", "clang", extra_args=extra_args)
llvm_config.add_tool_substitutions(subst)

tools = ["opt", "not", "FileCheck", "clang"]
llvm_config.add_tool_substitutions(tools, config.llvm_tools_dir)

config.substitutions.append(("%shlibdir", config.llvm_shlib_dir))
config.substitutions.append(("%shlibext", config.llvm_shlib_ext))
