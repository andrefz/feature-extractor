; RUN:  opt -load %shlibdir/libFeatureExtractor%shlibext --feature-extractor -disable-output 2>&1 %s\
; RUN:   | FileCheck %s
; RUN:  opt -load-pass-plugin  %shlibdir/libFeatureExtractor%shlibext -passes=feature-extractor -disable-output 2>&1 %s\
; RUN:   | FileCheck %s

define dso_local void @phi(i1 zeroext %0, i1 zeroext %1) #0 {
  %3 = alloca i8, align 1
  %4 = alloca i8, align 1
  %5 = alloca i8, align 1
  %6 = zext i1 %0 to i8
  store i8 %6, i8* %3, align 1
  %7 = zext i1 %1 to i8
  store i8 %7, i8* %4, align 1
  %8 = load i8, i8* %3, align 1
  %9 = trunc i8 %8 to i1
  br i1 %9, label %13, label %10
10:
  %11 = load i8, i8* %4, align 1
  %12 = trunc i8 %11 to i1
  br label %13
13:
  %14 = phi i1 [ true, %2 ], [ %12, %10 ]
  %15 = zext i1 %14 to i8
  store i8 %15, i8* %5, align 1
  ret void
}

define dso_local void @binary_integer(i32 %0, i32 %1) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  %9 = load i32, i32* %3, align 4
  %10 = load i32, i32* %4, align 4
  %11 = add nsw i32 %9, %10
  store i32 %11, i32* %5, align 4
  %12 = load i32, i32* %3, align 4
  %13 = load i32, i32* %4, align 4
  %14 = sub nsw i32 %12, %13
  store i32 %14, i32* %6, align 4
  %15 = load i32, i32* %3, align 4
  %16 = load i32, i32* %4, align 4
  %17 = mul nsw i32 %15, %16
  store i32 %17, i32* %7, align 4
  %18 = load i32, i32* %3, align 4
  %19 = load i32, i32* %4, align 4
  %20 = sdiv i32 %18, %19
  store i32 %20, i32* %8, align 4
  ret void
}

define dso_local void @binary_float(float %0, float %1) #0 {
  %3 = alloca float, align 4
  %4 = alloca float, align 4
  %5 = alloca float, align 4
  %6 = alloca float, align 4
  %7 = alloca float, align 4
  %8 = alloca float, align 4
  store float %0, float* %3, align 4
  store float %1, float* %4, align 4
  %9 = load float, float* %3, align 4
  %10 = load float, float* %4, align 4
  %11 = fadd float %9, %10
  store float %11, float* %5, align 4
  %12 = load float, float* %3, align 4
  %13 = load float, float* %4, align 4
  %14 = fsub float %12, %13
  store float %14, float* %6, align 4
  %15 = load float, float* %3, align 4
  %16 = load float, float* %4, align 4
  %17 = fmul float %15, %16
  store float %17, float* %7, align 4
  %18 = load float, float* %3, align 4
  %19 = load float, float* %4, align 4
  %20 = fdiv float %18, %19
  store float %20, float* %8, align 4
  ret void
}

define dso_local i32* @pfunc(i32 %0, i32 %1) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32*, align 8
  %6 = alloca i32*, align 8
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  store i32* %3, i32** %5, align 8
  store i32* %4, i32** %6, align 8
  %7 = load i32*, i32** %5, align 8
  %8 = getelementptr inbounds i32, i32* %7, i64 3
  store i32* %8, i32** %5, align 8
  %9 = load i32*, i32** %6, align 8
  %10 = getelementptr inbounds i32, i32* %9, i64 -3
  store i32* %10, i32** %6, align 8
  %11 = load i32*, i32** %5, align 8
  ret i32* %11
}

define dso_local i32 @args(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4) #0 {
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  store i32 %0, i32* %6, align 4
  store i32 %1, i32* %7, align 4
  store i32 %2, i32* %8, align 4
  store i32 %3, i32* %9, align 4
  store i32 %4, i32* %10, align 4
  %11 = load i32, i32* %6, align 4
  ret i32 %11
}

define dso_local i32 @ptrarg(i32* %0) #0 {
  %2 = alloca i32*, align 8
  store i32* %0, i32** %2, align 8
  %3 = load i32*, i32** %2, align 8
  %4 = load i32, i32* %3, align 4
  ret i32 %4
}

define dso_local i32 @main() #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i32* (i32, i32)*, align 8
  %5 = alloca i32*, align 8
  store i32 0, i32* %1, align 4
  store i32* @main.s1, i32** %2, align 8
  %6 = load i32*, i32** %2, align 8
  %7 = call i32 @ptrarg(i32* %6)
  %8 = call i32 @args(i32 0, i32 5, i32 15, i32 20, i32 30)
  store i32 %8, i32* %3, align 4
  store i32* (i32, i32)* @pfunc, i32* (i32, i32)** %4, align 8
  %9 = call i32* @pfunc(i32 1, i32 2)
  store i32* %9, i32** %5, align 8
  %10 = load i32, i32* %3, align 4
  ret i32 %10
}
