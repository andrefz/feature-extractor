#include <llvm/IR/Module.h>
#include <nlohmann/json.hpp>
#include <ostream>

using namespace llvm;

using json = nlohmann::json;

namespace feature_extractor {

class FeatureExtractor {
protected:
  json features;

public:
  FeatureExtractor(Module &);
  friend std::ostream &operator<<(std::ostream &, const FeatureExtractor &);
};

} // namespace feature_extractor
